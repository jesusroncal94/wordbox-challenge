# You arrive at the Venus fuel depot only to discover it's protected by a password. The Elves had written the password on a sticky note, but someone threw it out.

# However, they do remember a few key facts about the password:

#     It is a six-digit number.
#     The value is within the range given in your puzzle input.
#     Two adjacent digits are the same (like 22 in 122345).
#     Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).

# Other than the range rule, the following are true:

#     111111 meets these criteria (double 11, never decreases).
#     223450 does not meet these criteria (decreasing pair of digits 50).
#     123789 does not meet these criteria (no double).

# How many different passwords within the range given in your puzzle input meet these criteria?


def validate(num: int, rules: list) -> bool:
    """[summary]

    Args:
        num (int): Number to validate.
        rules (list): List of rules.

    Returns:
        bool: Describes if the number complies with the rules.
    """

    # Return True if number complies with the rules
    return all(rule(str(num)) for rule in rules)


def get_num_of_passwords(bounds: list[int], rules: list) -> int:
    """[summary]

    Args:
        bounds (list[int]): Limits search range.
        rules (list): List of rules.

    Returns:
        int: Sum of different passwords within the range given.
    """

    # Initialize variables
    init = bounds[0]
    end = bounds[1] + 1

    # Return and sum the cases that comply with the rules
    return sum(validate(num, rules) for num in range(init, end))


try:
    bounds = [100000, 999999]
    rules = [
        # Validate that digits are never decreasing
        lambda s: all(a <= b for a, b in zip(s, s[1:])),
        # Validate that two adjacent digits are equal
        lambda s: any(a == b for a, b in zip(s, s[1:]))
    ]
    print(get_num_of_passwords(bounds, rules))
except Exception as exc:
    print(f"{exc.__class__.__name__} Exception: [{exc}]")
