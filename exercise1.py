# There is an array of n integers. There are also 2 disjoint sets, A and B each containing
# m integers. You like all the integers in set A and dislike all the integers in set B. Your
# initial happiness is 0. For each i integer in the array, if i ∈ A, you add 1 to your
# happiness. If i ∈ B, you add -1 to your happiness. Otherwise, your happiness does not
# change. Output your final happiness at the end.

# Note: Since A and B are sets, they have no repeated elements. However, the array
# might contain duplicate elements.

# Constraints
# 1 <= n <= 10^5
# 1 <= m <= 10^5
# 1 <= Any integer in the input <= 10^9

# Input Format
# The first line contains integers n and m separated by a space.
# The second line contains n integers, the elements of the array.
# The third and fourth lines contain m integers, A and B, respectively.

import sys
from typing import Union


def build_object(line: str, cls: Union[list, set] = set) -> Union[list[int], set[int]]:
    """Return a list or set of integers from valid line.

    Args:
        line (str): Contains a list of numbers separated by whitespace.
        cls (Union[list, set], optional): Defines if the return object is a set or a list. Defaults to set.

    Returns:
        Union[list[int], set[int]]: List or set of integers.
    """

    # Validate if class is set or list
    cls = set if issubclass(cls, set) else list if issubclass(cls, list) else sys.exit()
    # Build object from valid line by list or set comprehension
    # Validate second constraint
    obj = cls(int(x) if 1 <= int(x) <= pow(10, 9) else sys.exit() for x in line.split())

    return obj


try:
    # Read input from file
    input = []
    with open("input_exercise1.txt") as file:
        for line in file.readlines():
            input.append(line.replace("\n", ""))

    # Get n and m params
    n, m = [int(w) for w in input[0].split()]
    # Validate first constraint for n and m params
    for a in [n, m]:
        if not (1 <= a <= pow(10, 5)):
            sys.exit()

    # Get array, A and B params
    array = build_object(line=input[1], cls=list)
    A = build_object(line=input[2], cls=set)
    B = build_object(line=input[3], cls=set)

    # Validate happiness
    happiness = 0
    for value in array:
        tmp = 1 if value in A else -1 if value in B else 0
        happiness += tmp

    print(happiness)
except Exception as exc:
    print(f"{exc.__class__.__name__} Exception: [{exc}]")
