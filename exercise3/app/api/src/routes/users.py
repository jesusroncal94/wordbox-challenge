from api.src.controllers.users import UserAPI
from api.src.utils.router import register_api


register_api(UserAPI, 'user_api', '/users/', pk='userId', pk_type='uuid')
