from flask import current_app as app, jsonify


@app.route('/api/v1/')
def index():
    return jsonify(message='Flask API running successfully!')


from api.src.routes import users as _
