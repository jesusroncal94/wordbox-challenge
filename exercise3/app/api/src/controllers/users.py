from flask import current_app as app, jsonify, request
from werkzeug.exceptions import NotFound

from api.src.controllers import BaseAPI
from api.src.models import db, Phone, User
from api.src.schemas import PhoneSchema, UserSchema


class UserAPI(BaseAPI):
    config = app.config
    model = User
    schema = UserSchema()
