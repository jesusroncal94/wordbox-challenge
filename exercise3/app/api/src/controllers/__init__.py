from flask import current_app as app, jsonify, request
from flask.views import MethodView
from sqlalchemy import desc
from werkzeug.exceptions import MethodNotAllowed, NotFound

from api.src.models import db


class BaseAPI(MethodView):
    config = app.config
    model = None
    schema = None

    def get(self, **kwargs):
        # Initialize variables
        tablename = self.model.__tablename__
        id = kwargs.get(tablename[:1].lower() + tablename[1:] + 'Id')

        if not id:
            # Return a list of elements
            # Get pagination config
            page = request.args.get('page', self.config.get('DEFAULT_CURRENT_PAGE'), type=int)
            per_page = request.args.get('per_page', self.config.get('DEFAULT_PER_PAGE_SIZE'), type=int)

            # Search all Users and paginate
            result = self.model.query.order_by(desc(self.model.created)).paginate(
                page=page, per_page=per_page
            )

            # Serialize
            self.schema.many = True
            return jsonify(
                data=self.schema.dump(result.items),
                metadata={
                    'page': result.page,
                    'pages': result.pages,
                    'total': result.total,
                    'prev_num': result.prev_num,
                    'next_num': result.next_num,
                    'has_next': result.has_next,
                    'has_prev': result.has_prev
                })
        else:
            # Expose a single element
            # Search and validate User by id
            result = self.model.query.get(id)
            if not result:
                raise NotFound
            self.schema.many = False
            return jsonify(data=self.schema.dump(result))

    def post(self):
        # Initialize variables
        data = request.json
        self.schema.many = False

        # Validate and load data
        validated_data = self.schema.load(data)

        # Create element
        db.session.add(validated_data)
        db.session.commit()
        created = self.schema.dump(validated_data)

        return jsonify(data=created)

    def put(self, **kwargs):
        raise MethodNotAllowed

    def delete(self, **kwargs):
        raise MethodNotAllowed
