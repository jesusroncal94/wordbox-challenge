from flask import current_app as app
from flask_marshmallow import Marshmallow


ma = Marshmallow(app)


from api.src.schemas.phones import PhoneSchema
from api.src.schemas.users import UserSchema
