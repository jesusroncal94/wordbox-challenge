from api.src.models import Phone
from api.src.schemas import ma


class PhoneSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Phone
        include_fk = True
        load_instance = True
