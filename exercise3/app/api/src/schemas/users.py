from marshmallow_sqlalchemy.fields import Nested

from api.src.models import User
from api.src.schemas import ma, PhoneSchema


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User
        include_relationships = True
        load_instance = True

    phones = Nested(PhoneSchema, many=True)
