import uuid
from sqlalchemy.dialects.postgresql import UUID

from api.src.models import db, TimestampMixin


class User(TimestampMixin, db.Model):
    __tablename__ = 'User'

    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = db.Column(db.String(50), nullable=False)
    lastname = db.Column(db.String(50), nullable=False)
    address = db.Column(db.String(200), default=True)
    phones = db.relationship('Phone', backref='User')

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.id} | {self.name}>'


class Phone(TimestampMixin, db.Model):
    __tablename__ = 'Phone'

    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    description = db.Column(db.String(50), nullable=False)
    phone = db.Column(db.String(20), nullable=False)
    userId = db.Column(UUID(as_uuid=True), db.ForeignKey('User.id'))

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.id} | {self.phone}>'
