from datetime import datetime
from flask import current_app as app
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy(app)


class TimestampMixin():
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated = db.Column(db.DateTime, onupdate=datetime.utcnow)


from api.src.models.users import User, Phone
