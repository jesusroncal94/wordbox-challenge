from flask import Flask
from flask_cors import CORS

from config import Config


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)
    _ = CORS(app, resources={r"/api/*": {"origins": "*"}})

    with app.app_context():
        from api.src.models import db
        from api.src import routes as _

        db.create_all()

        return app
