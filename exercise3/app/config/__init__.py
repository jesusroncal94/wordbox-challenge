import os
from distutils.util import strtobool


class Config():
    """Configuration for the API."""
    SECRET_KEY = os.getenv('SECRET_KEY', 'foo')
    ENV = os.getenv('FLASK_ENV', 'development')
    DEBUG = bool(strtobool(os.getenv('FLASK_DEBUG', 'True')))
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI', 'postgresql://postgres@127.0.0.1:5432/wordbox')
    SQLALCHEMY_TRACK_MODIFICATIONS = bool(strtobool(os.getenv('SQLALCHEMY_TRACK_MODIFICATIONS', 'True')))
    DEFAULT_CURRENT_PAGE = int(os.getenv('DEFAULT_CURRENT_PAGE', 1))
    DEFAULT_PER_PAGE_SIZE = int(os.getenv('DEFAULT_PER_PAGE_SIZE', 10))
